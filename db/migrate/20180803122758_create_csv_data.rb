class CreateCsvData < ActiveRecord::Migration[5.2]
  def change
    create_table :csv_data do |t|
      t.string :text1
      t.string :text2
      t.string :text3

      t.timestamps
    end
  end
end
