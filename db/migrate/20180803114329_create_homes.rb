class CreateHomes < ActiveRecord::Migration[5.2]
  def change
    create_table :homes do |t|
      t.string :denomination
      t.attachment :csv_file

      t.timestamps
    end
  end
end
