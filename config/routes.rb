Rails.application.routes.draw do
  devise_for :users, path: 'users', controllers: { sessions: "users/sessions",registrations: "users/registrations",  passwords: "users/passwords", omniauth_callbacks: 'users/omniauth_callbacks'}
   root to: 'homes#index'
   resources :homes do
   	collection do
   		get :csv_exports
   	end
   end

   resources :csv_datums do
   	collection do
   		post :data_csv
   	end
   end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
