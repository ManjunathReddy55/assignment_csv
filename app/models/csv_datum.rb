class CsvDatum < ApplicationRecord
	require 'csv'
	def self.csv_import(file,sku_denomination)
		CSV.foreach(file.path, headers: true) do |row|
      data_hash = row.to_hash
      puts "---------->>>>    #{data_hash}"
      csv_data = CsvDatum.new
      csv_data.text1 = data_hash["option1_sku"]
      csv_data.text2 = data_hash["option2_sku"]
      csv_data.text3 = data_hash["option3_sku"]
      csv_data.sku_denomination = sku_denomination
      csv_data.save
    end
	end


	def self.csv_export(csv_data)
		CSV.generate do |csv|
		  csv << ["sku_denomination", "sku_combination"]

		  csv_data.each do |stu|
		    csv << [stu.sku_denomination, stu.text1 + "," + stu.text2 + "," + stu.text3 ]
		  end
		end
	end
end
	