class CsvDatumsController < ApplicationController

	def data_csv
		sku_denomination = params[:sku_denomination]
		CsvDatum.csv_import(params[:file],sku_denomination)
		redirect_to :root
	end
end
