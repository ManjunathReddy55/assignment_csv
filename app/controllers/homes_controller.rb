class HomesController < ApplicationController
  before_action :set_sku, only: [:show, :edit, :update, :destroy]
  def index
  	@homes = Home.new
  end
  
  def show;end

  def edit;end

  def update;end

  def destroy;end

  def create
  	@homes = Home.new(homes_params)
  	puts "==================#{@homes.inspect}"
  	if @homes.save
  		flash[:sucess] = "New Denomination Created"
  		redirect_to homes_path
  	else
  		flash[:error] = "New Denomination not Created"
  		redirect_to homes_path	
  	end
  end

  def csv_data
  end	

  def csv_exports
  	if params[:csv_exports].present?
  		@all_data = CsvDatum.all
  		respond_to do |format|
        format.html
        format.csv { send_data CsvDatum.csv_export(@all_data), filename: "#{Date.today}.csv" }
      end
  	end
  end

  private

  def sku
  	@homes = Home.find(params[:id])
  end

  def homes_params
  	params.require(:home).permit(:denomination, :csv_file)
  end
end
